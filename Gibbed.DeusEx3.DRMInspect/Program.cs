﻿using System;
using System.Collections.Generic;
using System.IO;
using Gibbed.DeusEx3.FileFormats;
using NDesk.Options;

namespace Gibbed.DeusEx3.DRMInspect
{
    class MainClass
    {
        private static string GetExecutableName()
        {
            return Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
        }

        public static void Main(string[] args)
        {
            bool showHelp = false;

            var options = new OptionSet()
            {
                { "h|help", "show this message and exit", v => showHelp = v != null },
            };

            List<string> extras;

            try
            {
                extras = options.Parse(args);
            }
            catch (OptionException e)
            {
                Console.Write("{0}: ", GetExecutableName());
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `{0} --help' for more information.", GetExecutableName());
                return;
            }

            if (extras.Count != 1 ||
                showHelp == true)
            {
                Console.WriteLine("Usage: {0} [OPTIONS]+ input_file.drm", GetExecutableName());
                Console.WriteLine();
                Console.WriteLine("Options:");
                options.WriteOptionDescriptions(Console.Out);
                return;
            }

            string inputPath = extras[0];
            string outputPath = extras.Count > 1 ? extras[1] : Path.ChangeExtension(inputPath, null) + "_unpack";

            var drm = new DrmFile();
            using (var input = File.OpenRead(inputPath))
            {
                drm.Deserialize(input);
            }

            Console.WriteLine("DRM:");
            Console.WriteLine(String.Format("\tEndianness: {0}", drm.Endian));
            Console.WriteLine(String.Format("\tVersion: {0}", drm.Version));
            Console.WriteLine(String.Format("\tFlags: {0}", drm.Flags));
            Console.WriteLine(String.Format("\tUnknown0C: {0}", drm.unknown0C));
            Console.WriteLine(String.Format("\tUnknown10: {0}", drm.unknown10));
            Console.WriteLine(String.Format("\tUnknown1C: {0}", drm.unknown1C_Count));
            Console.WriteLine();

            Console.WriteLine(String.Format("\tUnknown04s ({0}):", drm.Unknown04s.Count));
            foreach (var u04 in drm.Unknown04s) {
                Console.WriteLine(String.Format("\t\t{0}", u04));
            }
            Console.WriteLine();

            Console.WriteLine(String.Format("\tUnknown08s ({0}):", drm.Unknown08s.Count));
            foreach (var u08 in drm.Unknown08s)
            {
                Console.WriteLine(String.Format("\t\t{0}", u08));
            }
            Console.WriteLine();

            Directory.CreateDirectory(outputPath);

            foreach (var section in drm.Sections)
            {
                Console.WriteLine(String.Format("{0}:", section.Id));
                Console.WriteLine(String.Format("\tData size: {0}", section.Data.Length));
                Console.WriteLine(String.Format("\tType: {0}", section.Type));
                Console.WriteLine(String.Format("\tFlags: {0}", section.Flags));
                Console.WriteLine(String.Format("\tUnknown05: {0}", section.Unknown05));
                Console.WriteLine(String.Format("\tUnknown06: {0}", section.Unknown06));
                Console.WriteLine(String.Format("\tUnknown10: {0}", section.Unknown10));
            }
        }
    }
}
