﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Gibbed.CrystalDynamics.FileFormats;
using NDesk.Options;

namespace Gibbed.DeusEx3.Inspect
{
    class MainClass
    {
        private static string GetExecutableName()
        {
            return Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
        }

        public static void Main(string[] args)
        {
            string filterPattern = null;
            string currentProject = null;
            bool showHelp = false;

            var options = new OptionSet()
            {
                { "f|filter=", "filter files using pattern", v => filterPattern = v },
                { "h|help", "show this message and exit", v => showHelp = v != null },
                { "p|project=", "override current project", v => currentProject = v },
            };

            List<string> extras;

            try
            {
                extras = options.Parse(args);
            }
            catch (OptionException e)
            {
                Console.Write("{0}: ", GetExecutableName());
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `{0} --help' for more information.", GetExecutableName());
                return;
            }

            if (extras.Count != 1 ||
                showHelp == true ||
                Path.GetExtension(extras[0]) != ".000")
            {
                Console.WriteLine("Usage: {0} [OPTIONS]+ input_file.000", GetExecutableName());
                Console.WriteLine();
                Console.WriteLine("Options:");
                options.WriteOptionDescriptions(Console.Out);
                return;
            }

            string inputPath = extras[0];

            Regex filter = null;
            if (string.IsNullOrEmpty(filterPattern) == false)
            {
                filter = new Regex(filterPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            }

            var manager = ProjectData.Manager.Load(currentProject);
            if (manager.ActiveProject == null)
            {
                Console.WriteLine("Warning: no active project loaded.");
            }

            var big = new BigArchiveFileV2();
            using (var input = File.OpenRead(inputPath))
            {
                big.Deserialize(input);
            }

            // Print info about bigfile itself
            Console.WriteLine("BIGFILE:");
            Console.WriteLine(String.Format("\tEndianness: {0}", big.Endian));
            Console.WriteLine(String.Format("\tAlignment: {0}", big.DataAlignment));
            Console.WriteLine(String.Format("\tBase path: {0}", big.BasePath));
            Console.WriteLine();

            var hashes = manager.LoadLists("*.filelist",
                                           s => s.HashFileName(),
                                           s => s.ToLowerInvariant());
            
            foreach (var entry in big.Entries.OrderBy(e => e.Offset))
            {
                string name = hashes[entry.NameHash];
                if (name == null)
                {
                    // Detecting extensions would slow this down, so let's just
                    // give everything without a hash .unknown
                    name = entry.NameHash.ToString("X8");
                    name = Path.ChangeExtension(name, "." + "unknown");
                    name = Path.Combine("unknown", name);
                    name = Path.Combine("__UNKNOWN", name);
                }
                else
                {
                    name = name.Replace("/", "\\");
                    if (name.StartsWith("\\") == true)
                    {
                        name = name.Substring(1);
                    }
                }

                // Convert backslashes to platform-specific path delimiter
                name = Path.Combine(name.Split('\\'));

                if (entry.Locale == 0xFFFFFFFF)
                {
                    name = Path.Combine("default", name);
                }
                else
                {
                    name = Path.Combine(entry.Locale.ToString("X8"), name);
                }

                if (filter != null &&
                    filter.IsMatch(name) == false)
                {
                    continue;
                }

                Console.WriteLine(String.Format("{0}:", name));
                Console.WriteLine(String.Format("\tName hash: {0}", entry.NameHash));
                Console.WriteLine(String.Format("\tUncompressed size: {0}", entry.UncompressedSize));
                Console.WriteLine(String.Format("\tCompressed size: {0}", entry.CompressedSize));
                Console.WriteLine(String.Format("\tOffset: {0}", entry.Offset));
                Console.WriteLine();
            }
        }
    }
}
