This is a fork of Rick (Gibbed)'s tools. The original repo is in SVN here:
http://svn.gib.me/public/crystaldynamics/

Don't forget to update git submodules after cloning the repo.

Original readme follows:

Uses icons from the 'Fugue' pack available at:
  http://p.yusukekamiyamane.com/

If you are not on Windows, you might need to comment out line 149 of
Gibbed.ProjectData/Project.cs ("throw;"), so that failure to read a registry
key is ignored.
